# Testing JavaScript App

Tutorial setup repo berikut

## Setup Test Runner

Kita bakal menggunakan [uvu](https://github.com/lukeed/uvu) dan [c8](https://github.com/bcoe/c8), test runner dan code coverage favorit saya yang paling simpel dan performan.

```bash
$ npm i --save-dev uvu c8
```

Setelah itu pasang pada package.json bagian script seperti berikut

```json
    "test": "uvu tests",
    "test:coverage": "c8 --include=src npm test",
    "test:report": "c8 report --reporter=text-lcov > coverage.lcov"
```

Jika sudah buat folder `tests` pada _root_ (folder paling luar) project, lalu mulai tulis test anda. Contoh buatlah file `test_contoh.js` pada folder `tests` dan copy -- paste kode berikut.

```js
// contoh menggunakan test
const { test } = require("uvu");
const assert = require("uvu/assert");

function sum(a, b) {
  return a + b;
}

function greet(name) {
  return `Hallo, ${name}`;
}

test("test sum function.", () => {
  assert.is(sum(1, 3), 4);
  assert.is(sum(10, 3), 13);
  assert.is(sum(11, 3), 14);
});

test("test greet function.", () => {
  assert.is(greet("Anton"), "Hallo, Anton");
  assert.is(greet("Tiska"), "Hallo, Tiska");
});

// Jangan lupa test.run() untuk menjalankan test runner
test.run();
```

Jalankan test dengan

```js
$ npm run test
// atau jika perlu melihat coverage
$ npm run test:coverage
```
