const { test } = require("uvu");
const assert = require("uvu/assert");

const { lowercaseAll, parseTime, sum } = require("../src/simple-function");

test("test sum function.", () => {
  assert.is(sum(1, 3), 4);
  assert.is(sum(10, 3), 13);
  assert.is(sum(11, 3), 14);
  assert.is(sum(10, 10), 20);
});

test("test lowercaseAll function.", () => {
  assert.equal(lowercaseAll(["YOKOHAMA", "SIMSON"]), ["yokohama", "simson"]);
  assert.equal(lowercaseAll(["DANI", "HESTI"]), ["dani", "hesti"]);
});

test("test parseTime function.", () => {
  assert.equal(parseTime("23:10:12"), {
    hours: "23",
    minutes: "10",
    seconds: "12",
  });

  assert.equal(parseTime("01:11:11"), {
    hours: "01",
    minutes: "11",
    seconds: "11",
  });
});

// Run test dont forget this, only on uvu
test.run();
