"use strict";

const { test } = require("uvu");
const assert = require("uvu/assert");
const build = require("../src/backend/app");

test("test root routes", async () => {
  const app = build();

  test("success get / route", async () => {
    const response = await app.inject({
      method: "GET",
      url: "/",
    });
    assert.equal(response.statusCode, 200);
    assert.equal(response.json(), { hello: "world" });
  });

  test.run();
});

test("CRUD routes", async () => {
  const app = build();
  const crud = crudHelper(app, "/todo");

  test("success create todo.", async () => {
    const response = await crud.create({ title: "Ke pasar beli jeruk" });
    assert.is(response.statusCode, 200);
    assert.equal(response.json(), ["Ke pasar beli jeruk"]);
    assert.is(response.json().length, 1);
  });

  test("failed create todo without tilte.", async () => {
    const response = await crud.create({ tittle: "Ke pasar beli jeruk" });
    assert.is(response.statusCode, 400);
    assert.is(response.json().message, "title belum di tulis");
  });

  test("success create todo again.", async () => {
    const response = await crud.create({ title: "Mencuci Mobil" });
    assert.is(response.statusCode, 200);
    assert.equal(response.json(), ["Ke pasar beli jeruk", "Mencuci Mobil"]);
    assert.is(response.json().length, 2);
  });

  test("update todo", async () => {
    const response = await crud.update(0, { title: "Ke pasar beli ayam" });
    assert.is(response.statusCode, 200);
    assert.is(response.json().length, 2);
    assert.equal(response.json(), ["Ke pasar beli ayam", "Mencuci Mobil"]);
  });

  test("get all todo.", async () => {
    const response = await crud.read();
    assert.is(response.statusCode, 200);
    assert.is(response.json().length, 2);
    assert.equal(response.json(), ["Ke pasar beli ayam", "Mencuci Mobil"]);
  });

  test("delete first todo.", async () => {
    const response = await crud.del(0);
    assert.is(response.statusCode, 200);
    assert.is(response.json().length, 1);
    assert.equal(response.json(), ["Mencuci Mobil"]);
  });

  test("get all todo after delete.", async () => {
    const response = await crud.read();
    assert.is(response.statusCode, 200);
    assert.is(response.json().length, 1);
    assert.equal(response.json(), ["Mencuci Mobil"]);
  });

  test.run();
});

test.run();

// Helper Function
function crudHelper(app, url) {
  async function create(payload) {
    const response = await app.inject({
      method: "POST",
      url,
      payload,
    });
    return response;
  }

  async function read() {
    const response = await app.inject({
      method: "GET",
      url,
    });
    return response;
  }

  async function update(id, payload) {
    const response = await app.inject({
      method: "PUT",
      url: `${url}/${id}`,
      payload,
    });
    return response;
  }

  async function del(id) {
    const response = await app.inject({
      method: "DELETE",
      url: `${url}/${id}`,
    });
    return response;
  }

  return { create, read, update, del };
}
