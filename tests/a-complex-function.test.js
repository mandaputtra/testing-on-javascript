const { test } = require("uvu");
const assert = require("uvu/assert");
const { bisaMasukGedung } = require("../src/a-complex-function");

test("test a-complex-function: bisaMasukGedung |", () => {
  test("tidak bisa masuk gedung pada hari minggu.", () => {
    const bisa = bisaMasukGedung(new Date("2022-01-02 10:10:10"));
    assert.is(bisa, false);
  });

  test("tidak bisa masuk gedung pada hari sabtu.", () => {
    const bisa = bisaMasukGedung(new Date("2022-01-01 10:10:10"));
    assert.is(bisa, false);
  });

  test("masuk jam 12 siang.", () => {
    const bisa = bisaMasukGedung(new Date("2022-01-04 12:10:10"));
    assert.is(bisa, true);
  });

  test("masuk jam 7 pagi.", () => {
    const bisa = bisaMasukGedung(new Date("2022-01-04 07:10:10"));
    assert.is(bisa, true);
  });

  test("masuk jam 9 malam.", () => {
    const bisa = bisaMasukGedung(new Date("2022-01-04 21:10:10"));
    assert.is(bisa, false);
  });

  test("masuk jam 8 malam.", () => {
    const bisa = bisaMasukGedung(new Date("2022-01-04 20:10:10"));
    assert.is(bisa, false);
  });

  test.run();
});
