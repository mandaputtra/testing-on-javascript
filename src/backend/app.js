"use strict";

const fastify = require("fastify");

const TODO_DB = [];

function build(opts = {}) {
  const app = fastify(opts);
  app.get("/", async function () {
    return { hello: "world" };
  });

  app.get("/todo", async function () {
    return TODO_DB;
  });

  app.post("/todo", async function (request, reply) {
    const { body } = request;
    if (!body.title) {
      return reply.code(400).send({ message: "title belum di tulis" });
    }
    TODO_DB.push(body.title);
    return TODO_DB;
  });

  app.put("/todo/:id", async function (request) {
    const { body, params } = request;
    TODO_DB[params.id] = body.title;
    return TODO_DB;
  });

  app.delete("/todo/:id", async function (request) {
    const { params } = request;
    TODO_DB.splice(params.id, 1);
    return TODO_DB;
  });

  return app;
}

module.exports = build;
