/**
 * BUSSINES FLOW / PERMASALAHAN :
 * Perusahaan melarang karyawan untuk memasuki gedung kantor pada hari sabtu dan minggu,
 * karyawan juga hanya bisa masuk gedung pada jam tertentu, 7 pagi sampai jam 6 sore.
 *
 * Untuk memudahkan perusahaan, programmer membuat alat absensi sebagai kunci pintunya.
 * Buatlah fungsi untuk mengecek apakah karyawan bisa masuk kantor atau tidak pada hari itu pada saat dia absen.
 *
 * Fungsi harus mengembalikan nilai boolean, true/false
 */

/**
 * Mengecek apakah karyawan bisa masuk gedung pada tanggal dan waktu yang di tentukan
 * @param {Date} tanggal - berisi tanggal dan waktu karywan masuk gedung
 * @returns {boolean} false saat karyawan tidak bisa masuk gedung dan true saat karyawan bisa memasuki gedung
 */
function bisaMasukGedung(tanggal) {
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/getDay?retiredLocale=id
  // Cek apakah saat ini hari sabtu (6) atau minggu (0)
  const hari = tanggal.getDay();
  if (hari === 0) return false;
  if (hari === 6) return false;
  // Cek apakah masih diantara jam 7 pagi sampai 6 sore
  const jam = tanggal.getHours();
  if (jam >= 7 && jam < 18) {
    return true;
  } else {
    return false;
  }
}

module.exports = { bisaMasukGedung };
