/**
 * sum two number
 * @param {number} a
 * @param {number} b
 * @returns {number} sum of two number
 */
function sum(a, b) {
  return a + b;
}

/*
 * lower case all value on array of string
 * @param {string[]} strings - an array of string
 * @returns {string[]} all lowercase string
 */
function lowercaseAll(strings) {
  return strings.map((str) => str.toLowerCase());
}

/**
 *
 * parse 'hh:mm:ss' string to an time object
 * @typedef {Object} TimeObject
 * @property {string} hour
 * @property {string} minute
 * @property {string} seconds
 * @param {string} timeString - string that looks like theese 'hh:mm:ss' (ex: '21:04:20')
 * @returns {TimeObject}
 */
function parseTime(timeString) {
  const split = timeString.split(":");
  return {
    hours: split[0],
    minutes: split[1],
    seconds: split[2],
  };
}

module.exports = { sum, lowercaseAll, parseTime };
